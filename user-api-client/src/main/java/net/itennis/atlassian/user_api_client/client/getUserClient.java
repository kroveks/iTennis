package net.itennis.atlassian.user_api_client.client;

import net.itennis.atlassian.user_api_client.model.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "check-player-client", url = "http://localhost:8080")
public interface getUserClient {
    @GetMapping("/users/{id}")
    User getUser(@PathVariable long id);
}
