package net.itennis.atlassian.crud.service.impl;

import net.itennis.atlassian.crud.exception.NegativeArgumentsException;
import net.itennis.atlassian.crud.exception.coach.CoachNotFoundException;
import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.filter.FilterMapper;
import net.itennis.atlassian.crud.model.Coach;
import net.itennis.atlassian.crud.model.User;
import net.itennis.atlassian.crud.repository.CoachRepository;
import net.itennis.atlassian.crud.service.CoachService;
import net.itennis.atlassian.crud.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CoachServiceImpl implements CoachService {
    private final static Logger logger = LogManager.getLogger(CoachServiceImpl.class);
    private final CoachRepository coachRepository;
    private final UserService userService;
    private final FilterMapper<Coach> filterMapper;

    public CoachServiceImpl(CoachRepository coachRepository,
                            UserService userService,
                            FilterMapper<Coach> filterMapper
    ) {
        this.coachRepository = coachRepository;
        this.userService = userService;
        this.filterMapper = filterMapper;
    }
    public List<Coach> getAllCoaches(int page, int size) {
        if (size < 0 || page < 0) throw new NegativeArgumentsException(page, size);
        List<Coach> coachList = new ArrayList<>();
        logger.info("Trying to fetch list of coaches");
        // default "size" value is 0, so it means that with default parameters we fetch all coaches
        if (size == 0) {
            coachRepository.findAll()
                           .forEach(coachList::add);
        } else {
            coachRepository.findAll(PageRequest.of(page, size))
                           .forEach(coachList::add);
        }
        logger.info("{} coaches were fetched", coachList.size());
        return coachList;
    }

    @Override
    public List<Coach> getFilteredCoaches(List<Filter> filters, int page, int size) {
        if (page < 0 || size < 0) throw new NegativeArgumentsException(page,size);
        List<Coach> userList = new ArrayList<>();
        logger.info("Trying to fetch list of users");
        Specification<Coach> specification = filterMapper.map(filters);
        // default "size" value is 0, so it means that with default parameters we fetch all users
        if (size == 0) {
            coachRepository.findAll(specification)
                           .forEach(userList::add);
        } else {
            coachRepository.findAll(specification, PageRequest.of(page, size))
                           .forEach(userList::add);
        }
        logger.info("{} users were fetched", userList.size());
        return userList;
    }

    @Override
    public Coach getCoachById(long id) {
        logger.info("Trying to fetch coach with id: {}", id);
        Optional<Coach> coachOptional = coachRepository.findById(id);
        if (coachOptional.isPresent()) {
            logger.info("Coach with id: {} was successfully fetched", id);
            return coachOptional.get();
        } else {
            logger.error("Coach with id: {} was not found", id);
            throw new CoachNotFoundException(id);
        }
    }
    @Override
    @Transactional
    public long createCoach(long userId, Coach coach) {
        logger.info("Trying to create coach for user with id: {}", userId);
        User user = userService.getUserById(userId);

        user.setCoach(coach);
        coach.setUser(user);

        userService.updateUser(user);
        Coach createdCoach = coachRepository.save(coach);
        logger.info("Coach for User with id: {} was successfully created", userId);

        return createdCoach.getId();
    }

    @Override
    @Transactional
    public void updateCoach(Coach coach) {
        long id = coach.getId();
        logger.info("Trying to update coach with id: {}", id);
        coachRepository.save(coach);
        logger.info("Coach with id: {} was successfully updated", id);
    }

    @Override
    @Transactional
    public void deleteCoachById(long id) {
        logger.info("Trying to delete coach with id: {}", id);
        coachRepository.deleteById(id);
        logger.info("Coach with id: {} successfully deleted", id);
    }
}