package net.itennis.atlassian.crud.filter;

import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.filter.model.InnerFilter;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.List;

import static org.springframework.data.jpa.domain.Specification.where;

@Component
public class FilterMapper<T> {

    private final InnerFilterMapper<T> innerFilterMapper;

    public FilterMapper(InnerFilterMapper<T> innerFilterMapper) {
        this.innerFilterMapper = innerFilterMapper;
    }

    private Specification<T> mapListOfInnerFilters(String field, List<InnerFilter> innerFilterList) {
        Specification<T> specification = where(innerFilterMapper.map(field, innerFilterList.remove(0)));
        specification = innerFilterList.stream()
                                       .map(innerFilter -> innerFilterMapper.map(field,innerFilter))
                                       .reduce(specification, Specification::or);
        return specification;
    }

    public Specification<T> map(List<Filter> filterList) {
        Filter firstFilter = filterList.remove(0);
        Specification<T> specification = where(mapListOfInnerFilters(firstFilter.getField(),
                firstFilter.getInnerFilterList()));
        specification = filterList.stream()
                .map(filter -> mapListOfInnerFilters(filter.getField(), filter.getInnerFilterList()))
                .reduce(specification, Specification::and);
        return specification;
    }
}
