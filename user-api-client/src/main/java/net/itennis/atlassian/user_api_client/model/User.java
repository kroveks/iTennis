package net.itennis.atlassian.user_api_client.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class User {
    private long id;
    private String fullName;

    private String email;
    private String phone;
    private String sex;
    private String city;

    private String about;
    private String photo_link;
    private LocalDateTime created_at;
    private long created_by;
    private LocalDateTime updated_at;

    private long updated_by;
    private boolean active;

    private Coach coach;

    private Player player;

    @Override
    public String toString() {
        String playerString = (player != null) ? player.toString() : "null";
        String coachString = (coach != null) ? coach.toString() : "null";
        String idString = (id != 0) ? ("" + id) : "not managed";
        return "User {" +
                "id=" + idString +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", sex=" + sex +
                ", city='" + city + '\'' +
                ", about='" + about + '\'' +
                ", photo_link='" + photo_link + '\'' +
                ", created_at=" + created_at +
                ", created_by=" + created_by +
                ", updated_at=" + updated_at +
                ", updated_by=" + updated_by +
                ", active=" + active +
                ", player='" + playerString + '\'' +
                ", coach='" + coachString + '\'' +
                '}';
    }
}