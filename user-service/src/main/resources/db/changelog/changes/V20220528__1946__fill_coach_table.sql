INSERT INTO coaches (experience_years, user_id) VALUES
    (1, (SELECT id FROM users WHERE full_name = 'Freja Bun')),
    (2, (SELECT id FROM users WHERE full_name = 'Anna Bertuchi')),
    (3, (SELECT id FROM users WHERE full_name = 'Fatima Kadirova'));

UPDATE users AS u
    SET coach_id = ch.id
    FROM coaches AS ch
    WHERE u.id = ch.user_id;