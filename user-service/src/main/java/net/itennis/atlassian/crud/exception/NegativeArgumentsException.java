package net.itennis.atlassian.crud.exception;

public class NegativeArgumentsException extends RuntimeException {

    public NegativeArgumentsException(int page, int size) {
        super("Arguments must be non-negative, but got page: " + page +", size: " + size);
    }
}
