package net.itennis.atlassian.crud.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "sex")
    private String sex;

    @Column(name = "city")
    private String city;

    @Column(name = "about")
    private String about;

    @Column(name ="photo_link")
    private String photo_link;

    @Column(name = "created_at")
    private LocalDateTime created_at;

    @Column(name = "created_by")
    private long created_by;

    @Column(name = "updated_at")
    private LocalDateTime updated_at;

    @Column(name = "updated_by")
    private long updated_by;

    @Column(name = "active")
    private boolean active;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coach_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Coach coach;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "player_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private Player player;

    @Override
    public String toString() {
        String playerString = (player != null) ? player.toString() : "null";
        String coachString = (coach != null) ? coach.toString() : "null";
        String idString = (id != 0) ? ("" + id) : "not managed";
        return "User {" +
                "id=" + idString +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", sex=" + sex +
                ", city='" + city + '\'' +
                ", about='" + about + '\'' +
                ", photo_link='" + photo_link + '\'' +
                ", created_at=" + created_at +
                ", created_by=" + created_by +
                ", updated_at=" + updated_at +
                ", updated_by=" + updated_by +
                ", active=" + active +
                ", player='" + playerString + '\'' +
                ", coach='" + coachString + '\'' +
                '}';
    }
}