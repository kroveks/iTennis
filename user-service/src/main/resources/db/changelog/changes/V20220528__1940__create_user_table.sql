create table "users" (
                         id BIGSERIAL PRIMARY KEY NOT NULL,
                         full_name VARCHAR(255),
                         email VARCHAR(255),
                         phone VARCHAR(12),
                         sex VARCHAR(1),
                         city VARCHAR(255),
                         about VARCHAR(500),
                         photo_link VARCHAR(200),
                         created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
                         created_by bigint DEFAULT 1,
                         updated_at timestamp without time zone,
                         updated_by bigint DEFAULT 1,
                         active boolean DEFAULT true
);