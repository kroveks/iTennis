package net.itennis.atlassian.crud.exception.player;

public class PlayerNotFoundException extends RuntimeException {
    public PlayerNotFoundException(long id) {
        super("Player with id: " + id + " was not found");
    }
}
