package net.itennis.atlassian.crud.service;

import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.model.Coach;

import java.util.List;

public interface CoachService {
    List<Coach> getAllCoaches(int page, int size);
    List<Coach> getFilteredCoaches(List<Filter> filters, int page, int size);
    Coach getCoachById(long id);
    long createCoach(long userId, Coach coach);

    void updateCoach(Coach coach);
    void deleteCoachById(long id);
}