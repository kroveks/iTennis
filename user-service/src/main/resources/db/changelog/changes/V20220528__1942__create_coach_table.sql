create table "coaches" (
                           id BIGSERIAL PRIMARY KEY NOT NULL,
                           experience_years integer,
                           user_id BIGINT references users(id) ON DELETE CASCADE,
                           created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
                           created_by bigint DEFAULT 1,
                           updated_at timestamp without time zone,
                           updated_by bigint DEFAULT 1,
                           active boolean DEFAULT true
);