package net.itennis.atlassian.user_api_client.client;

import net.itennis.atlassian.user_api_client.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CheckCoachClient {

    private final RestTemplate restTemplate;
    private static final String URL = "http://localhost:8080/users/";
    @Autowired
    public CheckCoachClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public boolean checkCoach(long userId) {
        ResponseEntity<User> response = restTemplate.getForEntity(URL + userId, User.class);
        User user = response.getBody();
        return user.getCoach() != null ? true : false;
    }
}