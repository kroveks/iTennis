package net.itennis.atlassian.user_api_client.controller;

import net.itennis.atlassian.user_api_client.client.getUserClient;
import net.itennis.atlassian.user_api_client.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckPlayerController {

    private final getUserClient getUserClient;

    @Autowired
    public CheckPlayerController(getUserClient getUserClient) {
        this.getUserClient = getUserClient;
    }

    @GetMapping("/checkPlayer/{id}")
    public ResponseEntity<Boolean> checkPlayer(@PathVariable long id) {
        User user = getUserClient.getUser(id);
        return ResponseEntity.ok().body(user.getPlayer() != null ? true : false);
    }
}