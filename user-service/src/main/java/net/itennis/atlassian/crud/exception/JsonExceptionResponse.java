package net.itennis.atlassian.crud.exception;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class JsonExceptionResponse {

    private LocalDateTime timestamp;
    private String message;
}
