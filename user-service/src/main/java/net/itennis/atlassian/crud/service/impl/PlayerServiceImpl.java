package net.itennis.atlassian.crud.service.impl;

import net.itennis.atlassian.crud.exception.NegativeArgumentsException;
import net.itennis.atlassian.crud.exception.player.PlayerNotFoundException;
import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.filter.FilterMapper;
import net.itennis.atlassian.crud.model.Player;
import net.itennis.atlassian.crud.model.User;
import net.itennis.atlassian.crud.repository.PlayerRepository;
import net.itennis.atlassian.crud.service.PlayerService;
import net.itennis.atlassian.crud.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PlayerServiceImpl implements PlayerService {

    private final static Logger logger = LogManager.getLogger(PlayerServiceImpl.class);
    private final PlayerRepository playerRepository;
    private final UserService userService;

    private final FilterMapper<Player> filterMapper;

    public PlayerServiceImpl(PlayerRepository playerRepository,
                             UserService userService,
                             FilterMapper<Player> filterMapper
    ) {
        this.playerRepository = playerRepository;
        this.userService = userService;
        this.filterMapper = filterMapper;
    }
    @Override
    public List<Player> getAllPlayers(int page, int size) {
        if (page < 0 || size < 0) throw new NegativeArgumentsException(page, size);
        List<Player> playerList = new ArrayList<>();
        logger.info("Trying to fetch list of players");
        // default "size" value is 0, so it means that with default parameters we fetch all players
        if (size == 0) {
            playerRepository.findAll()
                            .forEach(playerList::add);
        } else {
            playerRepository.findAll(PageRequest.of(page, size))
                            .forEach(playerList::add);
        }
        logger.info("{} players were fetched", playerList.size());
        return playerList;
    }

    @Override
    public List<Player> getFilteredPlayers(List<Filter> filters, int page, int size) {
        if (page < 0 || size < 0) throw new NegativeArgumentsException(page,size);
        List<Player> playerList = new ArrayList<>();
        logger.info("Trying to fetch list of players");
        Specification<Player> specification = filterMapper.map(filters);
        // default "size" value is 0, so it means that with default parameters we fetch all users
        if (size == 0) {
            playerRepository.findAll(specification)
                            .forEach(playerList::add);
        } else {
            playerRepository.findAll(specification, PageRequest.of(page, size))
                            .forEach(playerList::add);
        }
        logger.info("{} users were fetched", playerList.size());
        return playerList;
    }

    @Override
    public Player getPlayerById(long id) {
        logger.info("Trying to fetch player with id: {}", id);
        Optional<Player> playerOptional = playerRepository.findById(id);
        if (playerOptional.isPresent()) {
            logger.info("Player with id: {} was successfully fetched", id);
            return playerOptional.get();
        } else {
            logger.error("Player with id: {} was not found", id);
            throw new PlayerNotFoundException(id);
        }
    }
    @Override
    @Transactional
    public long createPlayer(long userId, Player player) {
        logger.info("Trying to create player for user with id: {}", userId);
        User user = userService.getUserById(userId);

        user.setPlayer(player);
        player.setUser(user);

        userService.updateUser(user);
        Player createdPlayer = playerRepository.save(player);
        logger.info("Player for User with id: {} was successfully created", userId);

        return createdPlayer.getId();
    }

    @Override
    @Transactional
    public void updatePlayer(Player player) {
        long id = player.getId();
        logger.info("Trying to update player with id: {}", id);
        playerRepository.save(player);
        logger.info("Player with id: {} was successfully updated", id);
    }

    @Override
    @Transactional
    public void deletePlayerById(long id) {
        logger.info("Trying to delete player with id: {}", id);
        playerRepository.deleteById(id);
        logger.info("Player with id: {} successfully deleted", id);
    }
}