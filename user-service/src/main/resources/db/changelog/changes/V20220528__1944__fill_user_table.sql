INSERT INTO users (full_name, email, phone, sex, city, about, photo_link)
VALUES
    ('Jack Jackson', 'jack@mail.com', '14121249', 'M', 'New-York', 'I am Jack', 'www.pictures.com/139'),
    ('John Johnson', 'john@mail.com', '24124518', 'M', 'Ottawa', 'I am John', 'www.pictures.com/238'),
    ('Ivan Ivanov', 'ivan@mail.com', '34121247', 'M', 'Moscow', 'I am Ivan', 'www.pictures.com/337'),
    ('Sergey Sergeev', 'sergey@mail.com', '44121246', 'M', 'St. Petersburg', 'I am Sergey', 'www.pictures.com/436'),
    ('Miura Takahashi', 'miura@mail.com', '54121245', 'M', 'Tokyo', 'I am Miura', 'www.pictures.com/535'),
    ('Mary Moon', 'mary@mail.com', '64121244', 'F', 'London', 'I am Mary', 'www.pictures.com/634'),
    ('Nicole Gi', 'nicole@mail.com', '74121243', 'F', 'Paris', 'I am Nicole', 'www.pictures.com/733'),
    ('Freja Bun', 'freja@mail.com', '84121242', 'F', 'Berlin', 'I am Freja', 'www.pictures.com/832'),
    ('Anna Bertuchi', 'anna@mail.com', '94121241', 'F', 'Rome', 'I am Anna', 'www.pictures.com/931'),
    ('Fatima Kadirova', 'fatima@mail.com', '10121240', 'F', 'Grozny', 'I am Fatima', 'www.pictures.com/100');