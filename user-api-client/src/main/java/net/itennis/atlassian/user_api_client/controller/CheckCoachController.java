package net.itennis.atlassian.user_api_client.controller;

import net.itennis.atlassian.user_api_client.client.CheckCoachClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CheckCoachController {
    private final CheckCoachClient checkCoachClient;
    @Autowired
    public CheckCoachController(CheckCoachClient checkCoachClient) {
        this.checkCoachClient = checkCoachClient;
    }

    @GetMapping("/checkCoach/{id}")
    public ResponseEntity<Boolean> checkCoach(@PathVariable long id) {
        return ResponseEntity.ok().body(checkCoachClient.checkCoach(id));
    }
}
