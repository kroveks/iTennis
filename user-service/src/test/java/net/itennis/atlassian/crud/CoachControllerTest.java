package net.itennis.atlassian.crud;

import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.filter.model.InnerFilter;
import net.itennis.atlassian.crud.filter.model.QueryOperator;
import net.itennis.atlassian.crud.model.Coach;
import net.itennis.atlassian.crud.model.User;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CoachControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Order(1)
    void createCoachTest() {
        User jack = createJack();
        Coach coach = createJackCoach();

        Long id = restTemplate.postForEntity("/users/", jack, Long.class).getBody();
        ResponseEntity<Long> response = restTemplate.postForEntity("/coaches/?userId=" + id,
                coach,
                Long.class);

        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
        assertThat(response.getBody(), notNullValue());
    }
    @Test
    @Order(2)
    void getCoach() {
        ResponseEntity<Coach> response = restTemplate.getForEntity("/coaches/1", Coach.class);

        assertThat(response.getBody().getExperience_years(), is(10));
    }
    @Test
    @Order(3)
    void updateCoachTest() {
        Coach coach = createJohnCoach();
        coach.setId(1L);
        restTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        restTemplate.patchForObject("/coaches/1",
                coach,
                ResponseEntity.class);

        ResponseEntity<Coach> response = restTemplate.getForEntity("/coaches/1", Coach.class);
        assertThat(response.getBody().getExperience_years(), is(5));
    }
    @Test
    @Order(4)
    void deletePlayerTest() {
        restTemplate.delete("/coaches/1");

        ResponseEntity<Coach> response = restTemplate.getForEntity("/coaches/1", Coach.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }
    @Test
    @Order(5)
    void getCoachesListTest() {

        Long idJack = restTemplate.postForEntity("/users/", createJack(), Long.class).getBody();
        Long idJohn = restTemplate.postForEntity("/users/", createJohn(), Long.class).getBody();
        Coach johnCoach = createJohnCoach();
        Coach jackCoach = createJackCoach();

        restTemplate.postForEntity("/coaches/?userId=" + idJack,
                jackCoach,
                Long.class);
        restTemplate.postForEntity("/coaches/?userId=" + idJohn,
                johnCoach,
                Long.class);

        ResponseEntity<List<Coach>> response = restTemplate.exchange(
                "/coaches/",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        List<Coach> coaches = response.getBody();
        assertThat(coaches, hasSize(2));
        assertThat(coaches.get(0).getExperience_years(), is(10));
        assertThat(coaches.get(1).getExperience_years(), is(5));
    }

    @Test
    @Order(6)
    void getFilteredCoachesTest() {
        List<Filter> filterList = createFilter();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<List<Filter>> requestEntity = new HttpEntity<>(filterList, headers);
        ResponseEntity<List<Coach>> response = restTemplate.exchange("/coaches/filter",
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<>() {});
        List<Coach> coaches = response.getBody();
        assertThat(coaches, hasSize(1));
        assertThat(coaches.get(0).getExperience_years(), is(10));
    }

    private List<Filter> createFilter() {
        InnerFilter innerFilter = InnerFilter.builder().operator(QueryOperator.GREATER_THAN)
                .values(List.of("9"))
                .build();
        Filter filter = Filter.builder().field("experience_years")
                .innerFilterList(List.of(innerFilter))
                .build();
        return List.of(filter);
    }

    private User createJack() {
        return User.builder().fullName("Jack")
                .email("jack@mail.com")
                .phone("131847124")
                .sex("M")
                .city("London")
                .about("I'm Jack")
                .photo_link("www.image.com/1232")
                .created_at(LocalDateTime.now())
                .created_by(1L)
                .updated_at(null)
                .updated_by(0)
                .active(true)
                .coach(null)
                .player(null)
                .build();
    }

    private User createJohn() {
        return User.builder().fullName("John")
                .email("john@mail.com")
                .phone("13214547124")
                .sex("M")
                .city("New-York")
                .about("I'm John")
                .photo_link("www.image.com/192232")
                .created_at(LocalDateTime.now())
                .created_by(1L)
                .updated_at(null)
                .updated_by(0)
                .active(true)
                .coach(null)
                .player(null)
                .build();
    }

    private Coach createJackCoach() {
        return Coach.builder()
                .created_at(LocalDateTime.now())
                .active(true)
                .updated_at(null)
                .updated_by(0)
                .experience_years(10)
                .created_by(1L)
                .build();
    }

    private Coach createJohnCoach() {
        return Coach.builder()
                .created_at(LocalDateTime.now())
                .active(true)
                .updated_at(null)
                .updated_by(0)
                .experience_years(5)
                .created_by(1L)
                .build();
    }
}
