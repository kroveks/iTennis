package net.itennis.atlassian.crud.service.impl;

import net.itennis.atlassian.crud.exception.NegativeArgumentsException;
import net.itennis.atlassian.crud.exception.user.UserNotFoundException;
import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.filter.FilterMapper;
import net.itennis.atlassian.crud.model.User;
import net.itennis.atlassian.crud.repository.UserRepository;
import net.itennis.atlassian.crud.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final static Logger logger = LogManager.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final FilterMapper<User> filterMapper;

    public UserServiceImpl(UserRepository userRepository, FilterMapper<User> filterMapper) {
        this.userRepository = userRepository;
        this.filterMapper = filterMapper;
    }


    @Override
    public List<User> getAllUsers(int page, int size) {
        if (page < 0 || size < 0) throw new NegativeArgumentsException(page,size);
        List<User> userList = new ArrayList<>();
        logger.info("Trying to fetch list of users");
        // default "size" value is 0, so it means that with default parameters we fetch all users
        if (size == 0) {
            userRepository.findAll()
                          .forEach(userList::add);
        } else {
            userRepository.findAll(PageRequest.of(page, size))
                          .forEach(userList::add);
        }
        logger.info("{} users were fetched", userList.size());
        return userList;
    }

    @Override
    public List<User> getFilteredUsers(List<Filter> filters, int page, int size) {
        if (page < 0 || size < 0) throw new NegativeArgumentsException(page,size);
        List<User> userList = new ArrayList<>();
        logger.info("Trying to fetch list of users");
        Specification<User> specification = filterMapper.map(filters);
        // default "size" value is 0, so it means that with default parameters we fetch all users
        if (size == 0) {
                userRepository.findAll(specification)
                              .forEach(userList::add);
        } else {
                userRepository.findAll(specification, PageRequest.of(page, size))
                              .forEach(userList::add);
        }
        logger.info("{} users were fetched", userList.size());
        return userList;
    }

    @Override
    public User getUserById(long id) {
        logger.info("Trying to fetch user with id: {}", id);
        Optional<User> userOptional = userRepository.findById(id);
        if (userOptional.isPresent()) {
            logger.info("User with id: {} was successfully fetched", id);
            return userOptional.get();
        } else {
            logger.error("User with id: {} was not found", id);
            throw new UserNotFoundException(id);
        }
    }
    @Override
    @Transactional
    public long createUser(User user) {
        String name = user.getFullName();
        logger.info("Trying to create user with name: {}", name);
        User savedUser = userRepository.save(user);
        long id = savedUser.getId();
        logger.info("User with id: {} created", id);
        return id;
    }

    @Override
    @Transactional
    public void updateUser(User user) {
        long id = user.getId();
        logger.info("Trying to update user with id: {}", id);
        userRepository.save(user);
        logger.info("User with id: {} was successfully updated", id);
    }

    @Override
    @Transactional
    public void deleteUserById(long id) {
        logger.info("Trying to delete user with id: {}", id);
        userRepository.deleteById(id);
        logger.info("User with id: {} successfully deleted", id);
    }
}