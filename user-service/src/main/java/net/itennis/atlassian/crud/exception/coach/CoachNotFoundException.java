package net.itennis.atlassian.crud.exception.coach;

public class CoachNotFoundException extends RuntimeException {
    public CoachNotFoundException(long id) {
        super("Coach with id: " + id + " was not found");
    }
}
