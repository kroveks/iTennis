package net.itennis.atlassian.crud.controller;

import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.model.User;
import net.itennis.atlassian.crud.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final static Logger logger = LogManager.getLogger(UserController.class);
    private final UserService userService;
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/")
    public ResponseEntity<List<User>> getAllUsers(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "0") int size
    ) {
        List<User> userList = userService.getAllUsers(page, size);
        return ResponseEntity.ok().body(userList);
    }

    @PostMapping("/filter")
    public ResponseEntity<List<User>> filterUser(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "0") int size,
            @RequestBody List<Filter> filters
    ) {
        logger.info("Got filter: " + filters);
        List<User> userList = userService.getFilteredUsers(filters, page, size);
        return ResponseEntity.ok().body(userList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUser(@PathVariable("id") long id) {
        User user = userService.getUserById(id);
        logger.info("Got user: " + user);
        return ResponseEntity.ok().body(user);
    }

    @PostMapping("/")
    public ResponseEntity<Long> createUser(@RequestBody User user) {
        logger.info("Got user: " + user);
        Long id = userService.createUser(user);
        logger.info("User :" + user + " is created");
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<User> editUser(@RequestBody User user) {
        logger.info("Got user: " + user);
        userService.updateUser(user);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<User> deleteUser(@PathVariable("id") long id) {
        userService.deleteUserById(id);
        return ResponseEntity.ok().build();
    }
}