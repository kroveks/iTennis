package net.itennis.atlassian.crud.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "players")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "experience_years")
    private int experience_years;

    @Column(name = "created_at")
    private LocalDateTime created_at;

    @Column(name = "created_by")
    private long created_by;

    @Column(name = "updated_at")
    private LocalDateTime updated_at;

    @Column(name = "updated_by")
    private long updated_by;

    @Column(name = "active")
    private boolean active;

    @OneToOne(mappedBy = "player", cascade = CascadeType.REMOVE)
    @JoinColumn(name = "user_id")
    @JsonIgnore
    private User user;

    @Override
    public String toString() {
        String idString = (id == null || id != 0) ? ("" + id) : "not managed";
        return "Player {" +
                "id=" + idString +
                ", experience_years=" + experience_years +
                ", created_at=" + created_at +
                ", created_by=" + created_by +
                ", updated_at=" + updated_at +
                ", updated_by=" + updated_by +
                ", active=" + active +
                '}';
    }
}