package net.itennis.atlassian.crud.filter;

import net.itennis.atlassian.crud.exception.OperationNotSupported;
import net.itennis.atlassian.crud.filter.model.InnerFilter;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InnerFilterMapper<T> {

    public Specification<T> map(String field, InnerFilter innerFilter) {
        List<String> values = innerFilter.getValues();
        switch (innerFilter.getOperator()){
            case EQUALS:
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.equal(root.get(field), values.get(0));
            case NOT_EQUALS:
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.notEqual(root.get(field), values.get(0));
            case GREATER_THAN:
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.gt(root.get(field),
                                Long.valueOf(values.get(0)));
            case LESS_THAN:
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.lt(root.get(field),
                                Long.valueOf(values.get(0)));
            case LIKE:
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.like(root.get(field),
                                "%"+values.get(0)+"%");
            case BETWEEN:
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.between(root.get(field),
                                values.get(0), values.get(1));
            case IN:
                return (root, query, criteriaBuilder) ->
                        criteriaBuilder.in(root.get(field)).value(values);
            default:
                throw new OperationNotSupported();
        }
    }
}