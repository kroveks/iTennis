package net.itennis.atlassian.crud.exception;

public class OperationNotSupported extends RuntimeException{
    public OperationNotSupported() {
        super("Operation is not supported yet");
    }
}
