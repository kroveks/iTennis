package net.itennis.atlassian.crud.filter.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class Filter {
    private String field;
    private List<InnerFilter> innerFilterList;
}