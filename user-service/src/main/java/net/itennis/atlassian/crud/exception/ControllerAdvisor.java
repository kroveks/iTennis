package net.itennis.atlassian.crud.exception;

import net.itennis.atlassian.crud.exception.coach.CoachNotFoundException;
import net.itennis.atlassian.crud.exception.player.PlayerNotFoundException;
import net.itennis.atlassian.crud.exception.user.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

@RestControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public JsonExceptionResponse handleUserNotFoundException(UserNotFoundException ex) {
        return new JsonExceptionResponse(LocalDateTime.now(), ex.getMessage());
    }

    @ExceptionHandler(PlayerNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public JsonExceptionResponse handlePlayerNotFoundException(PlayerNotFoundException ex) {
        return new JsonExceptionResponse(LocalDateTime.now(), ex.getMessage());
    }

    @ExceptionHandler(CoachNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public JsonExceptionResponse handleCoachNotFoundException(CoachNotFoundException ex) {
        return new JsonExceptionResponse(LocalDateTime.now(), ex.getMessage());
    }

    @ExceptionHandler(NegativeArgumentsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonExceptionResponse handleNegativeArgumentsException(NegativeArgumentsException ex) {
        return new JsonExceptionResponse(LocalDateTime.now(), ex.getMessage());
    }

    @ExceptionHandler(OperationNotSupported.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public JsonExceptionResponse handleOperationNotSupported(OperationNotSupported ex) {
        return new JsonExceptionResponse(LocalDateTime.now(), ex.getMessage());
    }

}