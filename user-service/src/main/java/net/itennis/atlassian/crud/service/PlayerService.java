package net.itennis.atlassian.crud.service;

import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.model.Player;

import java.util.List;

public interface PlayerService {
    List<Player> getAllPlayers(int page, int size);
    List<Player> getFilteredPlayers(List<Filter> filters, int page, int size);
    Player getPlayerById(long id);
    long createPlayer(long userId, Player player);
    void updatePlayer(Player player);
    void deletePlayerById(long id);
}