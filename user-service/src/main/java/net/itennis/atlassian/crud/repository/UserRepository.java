package net.itennis.atlassian.crud.repository;

import net.itennis.atlassian.crud.model.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepository extends PagingAndSortingRepository<User, Long>,
                                        JpaSpecificationExecutor<User> {
}