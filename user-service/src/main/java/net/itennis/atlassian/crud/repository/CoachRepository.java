package net.itennis.atlassian.crud.repository;

import net.itennis.atlassian.crud.model.Coach;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CoachRepository extends PagingAndSortingRepository<Coach, Long>,
                                         JpaSpecificationExecutor<Coach> {
}