ALTER TABLE users
    ADD COLUMN player_id BIGINT references players(id) ON DELETE SET NULL;

ALTER TABLE users
    ADD COLUMN coach_id BIGINT references coaches(id) ON DELETE SET NULL;