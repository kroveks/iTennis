package net.itennis.atlassian.crud.controller;

import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.model.Player;
import net.itennis.atlassian.crud.service.PlayerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/players")
public class PlayerController {

    private final static Logger logger = LogManager.getLogger(PlayerController.class);
    private final PlayerService playerService;

    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @GetMapping("/")
    public ResponseEntity<List<Player>> getAllPlayers(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "0") int size
    ) {
        List<Player> playerList = playerService.getAllPlayers(page, size);
        return ResponseEntity.ok().body(playerList);
    }

    @PostMapping("/filter")
    public ResponseEntity<List<Player>> filterPlayer(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "0") int size,
            @RequestBody List<Filter> filters
    ) {
        logger.info("Got filter: " + filters);
        List<Player> playerList = playerService.getFilteredPlayers(filters, page, size);
        return ResponseEntity.ok().body(playerList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Player> getPlayer(@PathVariable("id") long id) {
        Player player = playerService.getPlayerById(id);
        logger.info("Got player: " + player);
        return ResponseEntity.ok().body(player);
    }

    @PostMapping("/")
    public ResponseEntity<Long> createPlayer(
            @RequestParam(name = "userId") long userId,
            @RequestBody Player player
    ) {
        logger.info("Got player: " + player);
        Long id = playerService.createPlayer(userId, player);
        logger.info("Player: " + player + " is created");
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Player> editPlayer(@RequestBody Player player) {
        logger.info("Got player: " + player);
        playerService.updatePlayer(player);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Player> deletePlayer(
            @PathVariable("id") long id
    ) {
        playerService.deletePlayerById(id);
        return ResponseEntity.ok().build();
    }
}