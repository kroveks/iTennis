package net.itennis.atlassian.crud.exception.user;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(long id) {
        super("User with id: " + id + " was not found");
    }
}
