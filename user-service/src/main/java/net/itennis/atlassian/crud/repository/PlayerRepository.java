package net.itennis.atlassian.crud.repository;

import net.itennis.atlassian.crud.model.Player;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PlayerRepository extends PagingAndSortingRepository<Player, Long>,
                                          JpaSpecificationExecutor<Player> {
}