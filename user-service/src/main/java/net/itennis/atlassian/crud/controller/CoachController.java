package net.itennis.atlassian.crud.controller;

import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.model.Coach;
import net.itennis.atlassian.crud.service.CoachService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/coaches")
public class CoachController {
    private final static Logger logger = LogManager.getLogger(CoachController.class);
    private final CoachService coachService;

    public CoachController(CoachService coachService) {
        this.coachService = coachService;
    }

    @GetMapping("/")
    public ResponseEntity<List<Coach>> getAllCoaches(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "0") int size
    ) {
        List<Coach> coachList = coachService.getAllCoaches(page, size);
        return ResponseEntity.ok().body(coachList);
    }

    @PostMapping("/filter")
    public ResponseEntity<List<Coach>> filterCoach(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "0") int size,
            @RequestBody List<Filter> filters
    ) {
        logger.info("Got filter: " + filters);
        List<Coach> coachList = coachService.getFilteredCoaches(filters, page, size);
        return ResponseEntity.ok().body(coachList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Coach> getCoach(@PathVariable("id") long id) {
        Coach coach = coachService.getCoachById(id);
        logger.info("Got coach: " + coach);
        return ResponseEntity.ok().body(coach);
    }

    @PostMapping("/")
    public ResponseEntity<Long> createCoach(
            @RequestParam(name = "userId") long userId,
            @RequestBody Coach coach
    ) {
        logger.info("Got coach: " + coach);
        Long id = coachService.createCoach(userId, coach);
        logger.info("Coach: " + coach + " is created");
        return new ResponseEntity<>(id, HttpStatus.CREATED);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<Coach> editCoach(@RequestBody Coach coach) {
        logger.info("Got coach: " + coach);
        coachService.updateCoach(coach);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Coach> deleteCoach(@PathVariable("id") long id) {
        coachService.deleteCoachById(id);
        return ResponseEntity.ok().build();
    }
}