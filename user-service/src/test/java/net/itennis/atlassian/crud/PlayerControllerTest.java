package net.itennis.atlassian.crud;

import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.filter.model.InnerFilter;
import net.itennis.atlassian.crud.filter.model.QueryOperator;
import net.itennis.atlassian.crud.model.Player;
import net.itennis.atlassian.crud.model.User;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PlayerControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Order(1)
    void createPlayerTest() {
        User jack = createJack();
        Player player = createJackPlayer();

        Long id = restTemplate.postForEntity("/users/", jack, Long.class).getBody();
        ResponseEntity<Long> response = restTemplate.postForEntity("/players/?userId=" + id,
                player,
                Long.class);

        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
        assertThat(response.getBody(), notNullValue());
    }
    @Test
    @Order(2)
    void getPlayer() {
        ResponseEntity<Player> response = restTemplate.getForEntity("/players/1", Player.class);

        assertThat(response.getBody().getExperience_years(), is(10));
    }
    @Test
    @Order(3)
    void updatePlayerTest() {
        Player player = createJohnPlayer();
        player.setId(1L);
        restTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        restTemplate.patchForObject("/players/1",
                player,
                ResponseEntity.class);

        ResponseEntity<Player> response = restTemplate.getForEntity("/players/1", Player.class);
        assertThat(response.getBody().getExperience_years(), is(5));
    }
    @Test
    @Order(4)
    void deletePlayerTest() {
        restTemplate.delete("/players/1");

        ResponseEntity<Player> response = restTemplate.getForEntity("/players/1", Player.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }
    @Test
    @Order(5)
    void getPlayersListTest() {

        Long idJack = restTemplate.postForEntity("/users/", createJack(), Long.class).getBody();
        Long idJohn = restTemplate.postForEntity("/users/", createJohn(), Long.class).getBody();
        Player johnPlayer = createJohnPlayer();
        Player jackPlayer = createJackPlayer();

        restTemplate.postForEntity("/players/?userId=" + idJack,
                jackPlayer,
                Long.class);
        restTemplate.postForEntity("/players/?userId=" + idJohn,
                johnPlayer,
                Long.class);

        ResponseEntity<List<Player>> response = restTemplate.exchange(
                "/players/",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        List<Player> players = response.getBody();
        assertThat(players, hasSize(2));
        assertThat(players.get(0).getExperience_years(), is(10));
        assertThat(players.get(1).getExperience_years(), is(5));
    }

    @Test
    @Order(6)
    void getFilteredPlayersTest() {
        List<Filter> filterList = createFilter();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<List<Filter>> requestEntity = new HttpEntity<>(filterList, headers);
        ResponseEntity<List<Player>> response = restTemplate.exchange("/players/filter",
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<>() {});
        List<Player> players = response.getBody();
        assertThat(players, hasSize(1));
        assertThat(players.get(0).getExperience_years(), is(10));
    }

    private List<Filter> createFilter() {
        InnerFilter innerFilter = InnerFilter.builder().operator(QueryOperator.GREATER_THAN)
                .values(List.of("9"))
                .build();
        Filter filter = Filter.builder().field("experience_years")
                .innerFilterList(List.of(innerFilter))
                .build();
        return List.of(filter);
    }

    private User createJack() {
        return User.builder().fullName("Jack")
                .email("jack@mail.com")
                .phone("131847124")
                .sex("M")
                .city("London")
                .about("I'm Jack")
                .photo_link("www.image.com/1232")
                .created_at(LocalDateTime.now())
                .created_by(1L)
                .updated_at(null)
                .updated_by(0)
                .active(true)
                .coach(null)
                .player(null)
                .build();
    }

    private User createJohn() {
        return User.builder().fullName("John")
                .email("john@mail.com")
                .phone("13214547124")
                .sex("M")
                .city("New-York")
                .about("I'm John")
                .photo_link("www.image.com/192232")
                .created_at(LocalDateTime.now())
                .created_by(1L)
                .updated_at(null)
                .updated_by(0)
                .active(true)
                .coach(null)
                .player(null)
                .build();
    }

    private Player createJackPlayer() {
        return Player.builder()
                .created_at(LocalDateTime.now())
                .active(true)
                .updated_at(LocalDateTime.now())
                .updated_by(0)
                .experience_years(10)
                .created_by(1L)
                .build();
    }

    private Player createJohnPlayer() {
        return Player.builder()
                .created_at(LocalDateTime.now())
                .active(true)
                .updated_at(null)
                .updated_by(0)
                .experience_years(5)
                .created_by(1L)
                .build();
    }
}