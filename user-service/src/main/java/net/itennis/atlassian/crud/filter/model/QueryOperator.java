package net.itennis.atlassian.crud.filter.model;

public enum QueryOperator {
    EQUALS,
    NOT_EQUALS,
    GREATER_THAN,
    LESS_THAN,
    LIKE,
    BETWEEN,
    IN
}
