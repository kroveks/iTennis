INSERT INTO players (experience_years, user_id)
VALUES
    (1, (SELECT id FROM users WHERE full_name = 'Jack Jackson')),
    (2, (SELECT id FROM users WHERE full_name = 'John Johnson')),
    (3, (SELECT id FROM users WHERE full_name = 'Ivan Ivanov')),
    (4, (SELECT id FROM users WHERE full_name = 'Sergey Sergeev')),
    (5, (SELECT id FROM users WHERE full_name = 'Miura Takahashi')),
    (6, (SELECT id FROM users WHERE full_name = 'Mary Moon')),
    (7, (SELECT id FROM users WHERE full_name = 'Nicole Gi'));

UPDATE users AS u
    SET player_id = p.id
    FROM players AS p
    WHERE u.id = p.user_id;