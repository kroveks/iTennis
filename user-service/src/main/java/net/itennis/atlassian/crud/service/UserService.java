package net.itennis.atlassian.crud.service;

import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.model.User;

import java.util.List;

public interface UserService {

    List<User> getAllUsers(int page, int size);
    List<User> getFilteredUsers(List<Filter> filters, int page, int size);
    User getUserById(long id);
    long createUser(User user);
    void updateUser(User user);
    void deleteUserById(long id);
}