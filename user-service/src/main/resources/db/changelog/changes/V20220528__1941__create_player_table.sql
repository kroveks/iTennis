create table "players" (
                           id BIGSERIAL PRIMARY KEY NOT NULL ,
                           user_id BIGINT references users(id) ON DELETE CASCADE ,
                           experience_years integer,
                           created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
                           created_by bigint DEFAULT 1,
                           updated_at timestamp without time zone,
                           updated_by bigint DEFAULT 1,
                           active boolean DEFAULT true
);