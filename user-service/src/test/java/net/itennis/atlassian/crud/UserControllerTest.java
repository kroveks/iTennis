package net.itennis.atlassian.crud;

import net.itennis.atlassian.crud.filter.model.Filter;
import net.itennis.atlassian.crud.filter.model.InnerFilter;
import net.itennis.atlassian.crud.filter.model.QueryOperator;
import net.itennis.atlassian.crud.model.User;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserControllerTest {
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    @Order(1)
    void createUserTest() {
        User user = createJack();

        ResponseEntity<Long> response = restTemplate.postForEntity("/users/", user, Long.class);

        assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
        assertThat(response.getBody(), notNullValue());
    }
    @Test
    @Order(2)
    void getUser() {
        ResponseEntity<User> response = restTemplate.getForEntity("/users/1", User.class);

        assertThat(response.getBody().getFullName(), is("Jack"));
    }
    @Test
    @Order(3)
    void updateUserTest() {
        User john = createJohn();
        john.setId(1L);
        restTemplate.getRestTemplate().setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        restTemplate.patchForObject("/users/1",
                john,
                ResponseEntity.class);

        ResponseEntity<User> response = restTemplate.getForEntity("/users/1", User.class);
        assertThat(response.getBody().getFullName(), is("John"));
    }
    @Test
    @Order(4)
    void deleteUserTest() {
        restTemplate.delete("/users/1");

        ResponseEntity<User> response = restTemplate.getForEntity("/users/1", User.class);
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
    }
    @Test
    @Order(5)
    void getUsersListTest() {

        restTemplate.postForEntity("/users/", createJack(), Long.class);
        restTemplate.postForEntity("/users/", createJohn(), Long.class);

        ResponseEntity<List<User>> response = restTemplate.exchange(
                "/users/",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<>() {
                });

        List<User> users = response.getBody();
        assertThat(users, hasSize(2));
        assertThat(users.get(0).getFullName(), is("Jack"));
        assertThat(users.get(1).getFullName(), is("John"));
    }

    @Test
    @Order(6)
    void getFilteredUsersTest() {
        List<Filter> filterList = createFilter();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<List<Filter>> requestEntity = new HttpEntity<>(filterList,headers);
        ResponseEntity<List<User>> response = restTemplate.exchange("/users/filter",
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<>() {});
        List<User> users = response.getBody();
        assertThat(users, hasSize(1));
        assertThat(users.get(0).getFullName(), is("Jack"));
    }

    private List<Filter> createFilter() {
        InnerFilter innerFilter1 = InnerFilter.builder().operator(QueryOperator.EQUALS)
                .values(List.of("New-York"))
                .build();
        InnerFilter innerFilter2 = InnerFilter.builder().operator(QueryOperator.EQUALS)
                .values(List.of("London"))
                .build();
        InnerFilter innerFilter3 = InnerFilter.builder().operator(QueryOperator.EQUALS)
                .values(List.of("Jack"))
                .build();
        Filter filter1 = Filter.builder().field("city")
                .innerFilterList(List.of(innerFilter1, innerFilter2))
                .build();
        Filter filter2 = Filter.builder().field("fullName")
                .innerFilterList(List.of(innerFilter3))
                .build();
        return List.of(filter1, filter2);
    }

    private User createJack() {
        return User.builder().fullName("Jack")
                .email("jack@mail.com")
                .phone("131847124")
                .sex("M")
                .city("London")
                .about("I'm Jack")
                .photo_link("www.image.com/1232")
                .created_at(LocalDateTime.now())
                .created_by(1L)
                .updated_at(null)
                .updated_by(0)
                .active(true)
                .coach(null)
                .player(null)
                .build();
    }

    private User createJohn() {
        return User.builder().fullName("John")
                .email("john@mail.com")
                .phone("13214547124")
                .sex("M")
                .city("New-York")
                .about("I'm John")
                .photo_link("www.image.com/192232")
                .created_at(LocalDateTime.now())
                .created_by(1L)
                .updated_at(null)
                .updated_by(0)
                .active(true)
                .coach(null)
                .player(null)
                .build();
    }
}
