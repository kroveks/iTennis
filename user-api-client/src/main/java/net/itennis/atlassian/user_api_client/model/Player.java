package net.itennis.atlassian.user_api_client.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class Player {

    private Long id;

    private int experience_years;
    private LocalDateTime created_at;
    private long created_by;
    private LocalDateTime updated_at;
    private long updated_by;
    private boolean active;

    private User user;

    @Override
    public String toString() {
        String idString = (id == null || id != 0) ? ("" + id) : "not managed";
        return "Player {" +
                "id=" + idString +
                ", experience_years=" + experience_years +
                ", created_at=" + created_at +
                ", created_by=" + created_by +
                ", updated_at=" + updated_at +
                ", updated_by=" + updated_by +
                ", active=" + active +
                '}';
    }
}